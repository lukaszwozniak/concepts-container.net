﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PK.Container
{
    class Container : IContainer
    {
        private Dictionary<Type, object> myFirstDictionary;

        public Container() // konstruktor mojej klasy Container
        {
            myFirstDictionary = new Dictionary<Type, object>(); // deklaracja wymaganego własnego słownika
        }

        #region Resolve
        public T Resolve<T>() where T : class
        {
            var anyVariable = Resolve(typeof(T));
            if (anyVariable != null) // Resolve 'typu' != null, zwróć T
            {
                return anyVariable as T;
            }

            throw new PK.Container.UnresolvedDependenciesException("There is an error with resolving ( public T Resolve<T> )");

        }
        public object Resolve(Type type)
        {
            if (!myFirstDictionary.ContainsKey(type)) // jeśli różne, to rzuć wyjątek
            {
                throw new PK.Container.UnresolvedDependenciesException("There is an error with resolving ( public object Resolve )");
            }

            return myFirstDictionary[type];
        }
        #endregion

        #region Register
        public void Register<T>(T impl) where T : class
        {
            myFirstDictionary[typeof(T).GetType()] = impl;

        }
        public void Register<T>(Func<T> provider) where T : class
        {

            myFirstDictionary[typeof(T).GetType()] = provider;
        }
        public void Register(System.Reflection.Assembly assembly)
        {
            var interfaceCount = assembly.GetTypes();


            foreach (var item in interfaceCount)
            {
                Register(item);

            }
        }
        public void Register(Type type)
        {
            foreach (var t in type.GetInterfaces())
            {
                if (!myFirstDictionary.ContainsKey(t)) // jeśli nie zawiera ciągu 't', dodaj "stwórz instancję od 'type', gdzie t to typ
                {

                    myFirstDictionary.Add(t, Activator.CreateInstance(type));
                }
            }
        }
        #endregion
    }
}
